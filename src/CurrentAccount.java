public class CurrentAccount extends Account{

    public CurrentAccount(String name, double balance) {

        super.setName(name);
        super.setBalance(balance);

    }
    @Override
    public void addInterest() {
        setBalance(getBalance() *  1.1);
    }
}
