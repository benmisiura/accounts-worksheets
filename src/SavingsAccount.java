public class SavingsAccount extends Account{

    public SavingsAccount(String name, double balance) {

        super.setName(name);
        super.setBalance(balance);

    }
    @Override
    public void addInterest() {
        setBalance(getBalance() *  1.4);
    }
}
