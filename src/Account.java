public abstract class Account implements Detailable{

    private double balance;
    private String name;

    private static double interestRate;

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public Account(){
        this.balance = 50;
        this.name = "Ben";

    }

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public boolean withdraw(double amount){
        if (balance - amount >= 0)
        {
            balance = balance - amount;
            return true;
        }

        return false;
    }

    public boolean withdraw(){
        return withdraw(20);

    }

    @Override
    public String getDetails(){
        return "placeholder";
    };


}
