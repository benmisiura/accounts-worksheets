public class TestInterfaces {
    public static void main(String[] args) {

        Detailable[] detailables = new Detailable[3];


        SavingsAccount savingsAccount = new SavingsAccount("savings",4);
        CurrentAccount currentAccount = new CurrentAccount("current", 6);
        HomeInsurance homeInsurance = new HomeInsurance(1000,50,100000);

        detailables[0] = savingsAccount;
        detailables[1] = currentAccount;
        detailables[2] = homeInsurance;

        for (Detailable d: detailables){
            if (d!=null){
                System.out.println(d.getDetails());
        }
        }


    }
}
