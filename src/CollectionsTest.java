import java.util.HashSet;

public class CollectionsTest {
    public static void main(String[] args) {

        HashSet<Account> accounts;
        accounts = new HashSet<Account>();

        SavingsAccount savingsAccount = new SavingsAccount("savings",4);
        CurrentAccount currentAccount = new CurrentAccount("current", 6);
        CurrentAccount currentAccount2 = new CurrentAccount("current2", 8);

        accounts.add(savingsAccount);
        accounts.add(currentAccount);
        accounts.add(currentAccount2);

        for (Account a: accounts){
            System.out.println(a.getName());
            System.out.println(a.getBalance());
            a.addInterest();
            System.out.println(a.getBalance());

        }

    }
}

