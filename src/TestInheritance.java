public class TestInheritance {
    public static void main(String[] args) {

        //Account[] accounts = new Account[3];

         //Account account = new Account(2,"default");
        SavingsAccount savingsAccount = new SavingsAccount("savings",4);
        CurrentAccount currentAccount = new CurrentAccount("current", 6);


//        accounts[0] = account;
//        accounts[1] = savingsAccount;
//        accounts[2] = currentAccount;

        Account[] accounts = new Account[2];
        accounts[0] = savingsAccount;
        accounts[1] = currentAccount;

/*        for (Account acc:accounts){
            acc.addInterest();
            System.out.println("name: " + acc.getName() + " balance: " + acc.getBalance());


        }*/

        for (int i = 0; i < accounts.length; i++){
            accounts[i].addInterest();
            System.out.println("name: " + accounts[i].getName() + " balance: " + accounts[i].getBalance());

        }
    }
}
